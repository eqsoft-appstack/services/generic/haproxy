#!/bin/sh

if [ ! -d "${cache_folder}/${test_name}" ]; then
  mkdir "${cache_folder}/${test_name}"
fi

cp "${tests_folder}/config/haproxy.cfg" "${cache_folder}/${test_name}/haproxy.cfg"

docker run --rm -d -v "${cache_folder}/${test_name}:/usr/local/etc/haproxy" -p "${free_port}:9000" --name "${test_container_name}" "${test_image}"

docker logs $test_container_name

sleep 2

# get stats as csv and simply grep on "pxname"
echo ""
echo "test stats and grep to pxname header"
ret=$(curl --silent -m 5 "http://${build_host}:${free_port}/stats?stats;csv;norefresh" | grep pxname)

echo $ret

if [ "$?" != "0" ] || [ -z "$ret" ] ; then
  log 3 "testing image not successfull!"
  exit 1
fi

log 5 "testing image successfull"
